/*
 * Program: Ch4Lab1.java
 * Programmer: Ray Osborne
 * Purpose: This program will draw a star
 */

import javax.swing.JApplet;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Color;

public class Ch4Lab1 extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Initialize the applet
	public void init() {
		setSize(400, 400);
	}

	// Method to draw star
	public void paint(Graphics g) {
		// Invoke paint method and set starting color
		super.paint(g);
		g.setColor(Color.BLUE);
		
		// Draw Star
		Polygon star = new Polygon();
		star.addPoint(100,  85);
		star.addPoint(175,  75);
		star.addPoint(200,  10);
		star.addPoint(225,  75);
		star.addPoint(300,  85);
		star.addPoint(250,  125);
		star.addPoint(260,  190);
		star.addPoint(200,  150);
		star.addPoint(140,  190);
		star.addPoint(150,  125);
		star.addPoint(100,  85);
		
		// Fill Star
		g.fillPolygon(star);
		
		// Label Star
		g.drawString("This is a star", 175, 225);
	}
}
