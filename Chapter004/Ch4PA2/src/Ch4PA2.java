/*
 * Program: Ch4PA2.java
 * Programmer: Ray Osborne
 * Purpose: This program will draw and label the Olympic Rings
 */

import javax.swing.JApplet;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Color;

public class Ch4PA2 extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Initialize the applet
	public void init() {
		setSize(350, 200);
	}

	// Method to draw star
	public void paint(Graphics g) {
		// Invoke paint method and set starting color
		super.paint(g);

		// Set Blue Ring color
		g.setColor(Color.blue);
		
		// Draw Blue Ring
		g.drawOval(100,  25,  50,  50);
		
		// Set Black Ring color
		g.setColor(Color.black);
		
		// Draw Black Ring
		g.drawOval(155,  25,  50,  50);
		
		// Set Red Ring color
		g.setColor(Color.red);
		
		// Draw Red Ring
		g.drawOval(210,  25,  50,  50);
		
		// Set Yellow Ring color
		g.setColor(Color.yellow);
		
		// Draw Yellow Ring
		g.drawOval(125,  55,  50,  50);
		
		// Set Green Ring color
		g.setColor(Color.green);
		
		// Draw Green Ring
		g.drawOval(180,  55,  50,  50);
		
		// Set text color
		g.setColor(Color.blue);
				
		// Label Rings
		g.drawString("The Olympic Rings", 125, 150);
	}
}
