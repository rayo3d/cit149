/*
 * Program: Ch4Lab3.java
 * Programmer: Ray Osborne
 * Purpose: This program will draw a magic wand
 */

import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.Color;
import java.awt.Container;

public class Ch4Lab3 extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Default method for project
	public static void main(String[] args) {
		// Run new container window
		new Ch4Lab3();
	}
	
	// Paint method for project
	public void paint(Graphics g) {
		// Invoke paint method and set starting color
		super.paint(g);
		g.setColor(Color.BLUE);
		
		// Create wand base
		g.fillRect(190, 180, 20, 180);
		
		// Change color for star
		g.setColor(Color.YELLOW);
		
		// Draw star
		Polygon star = new Polygon();
		star.addPoint(100,  115);
		star.addPoint(175,  105);
		star.addPoint(200,  40);
		star.addPoint(225,  105);
		star.addPoint(300,  115);
		star.addPoint(250,  155);
		star.addPoint(260,  220);
		star.addPoint(200,  180);
		star.addPoint(140,  220);
		star.addPoint(150,  155);
		star.addPoint(100,  115);
		
		// Fill Star
		g.fillPolygon(star);
	}
	
	public Ch4Lab3() {
		// Construct container for displa
		Container c = getContentPane();
		
		// Set background color
		c.setBackground(Color.RED);
		
		// Set container size, title, and visibility
		setSize(400,400);
		setTitle("Magic Wand");
		setVisible(true);
	}
}
