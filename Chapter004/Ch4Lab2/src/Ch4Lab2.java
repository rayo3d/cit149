/*
 * Program: Ch4Lab1.java
 * Programmer: Ray Osborne
 * Purpose: This program will draw an ant
 */

import javax.swing.JApplet;
import java.awt.Graphics;
import java.awt.Color;

public class Ch4Lab2 extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// Initialize the applet
	public void init() {
		setBackground(Color.CYAN);
		
		setSize(400, 200);
	}

	// Method to draw star
	public void paint(Graphics g) {
		// Set body and eye color
		g.setColor(Color.red);
		
		// Draw the head
		g.drawOval(50,  50,  75,  50);
		
		// Draw the Eye
		g.fillOval(75,  60,  15,  15);
		
		// Draw the filled body
		g.fillOval(125,  55, 50,  50);
		g.fillOval(175,  55, 50,  50);
		g.fillOval(225,  55, 50,  50);
		
		// Set the antennae and legs color
		g.setColor(Color.BLACK);
		
		// Draw antennae stems
		g.drawLine(72,  27,  72,  53);
		g.drawLine(94,  27,  94,  51);
		
		// Draw tops of antennae
		g.fillOval(67,  25,  11,  11);
		g.fillOval(89,  25,  11,  11);
		
		// Draw the legs
		g.drawLine(150,  102,  150,  180);
		g.drawLine(140,  102,  140,  160);
		g.drawLine(200,  102,  200,  180);
		g.drawLine(190,  102,  190,  160);
		g.drawLine(250,  102,  250,  180);
		g.drawLine(240,  102,  240,  160);
	}
}
