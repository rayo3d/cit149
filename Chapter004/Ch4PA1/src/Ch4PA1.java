/*
 * Program: Ch4PA1.java
 * Programmer: Ray Osborne
 * Purpose: This program will draw several circles getting brighter similar to a bullseye
 */

import javax.swing.JApplet;
import java.awt.Graphics;
import java.awt.Color;

public class Ch4PA1 extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Initialize the applet
	public void init() {
		setSize(400, 400);
	}

	// Method to draw graphic
	public void paint(Graphics g) {
		// Set Dark Blue circle color
		g.setColor(new Color(25, 100, 150));
		
		// Draw Dark Blue circle
		g.fillOval(75,  75,  250,  250);
		
		// Set Blue circle color
		g.setColor(new Color(35, 142, 214));
		
		// Draw Blue circle
		g.fillOval(105,  105,  190,  190);
		
		// Set Light Blue circle color
		g.setColor(new Color(50, 202, 255));
		
		// Draw Light Blue circle
		g.fillOval(135,  135,  130,  130);
		
		// Set Very Light Blue circle color
		g.setColor(new Color(71, 255, 255));
		
		// Draw Very Light Blue circle
		g.fillOval(165,  165,  70,  70);
	}
}
