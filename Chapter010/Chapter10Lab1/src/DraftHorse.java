/**
 * 
 */

/**
 * @author RayOsborne
 * @date 12-1-2015
 * @purpose Extend Horse class to include information about Draft Horse information
 */
public class DraftHorse extends Horse {
	// Declare variables needed for DraftHorse class
	private String classification;
	private String draftBreed;
	
	// Draft Horse constructor method
	public DraftHorse () {
		
	}
	
	// Horse overloaded constructor method
	public DraftHorse (int anIdNo, String aCoatColor, int anAge, double aHeight, String aClassification, String aDraftBreed) {
		// Call super class constructor
		super(anIdNo, aCoatColor, anAge, aHeight);
		
		// Call subclass mutators
		setClassification(aClassification);
		setDraftBreed(aDraftBreed);
	}
	
	// Subclass Mutator methods
	public void setClassification(String aClassification) {
		classification = aClassification;
	}
	public void setDraftBreed(String aDraftBreed) {
		draftBreed = aDraftBreed;
	}
	
	// Subclass Accessor methods
	public String getClassification() {
		return classification;
	}
	public String getDraftBreed() {
		return draftBreed;
	}
	
	// Display Draft Horse subclass information
	public String tellAboutHorse() {
		// Declare variables needed for tellAboutHorse method
		String horseDetails;
		
		// Build return string
		horseDetails = String.format("%s%n%n%s%n%s%n%n%s%n%ss", super.tellAboutHorse(), "Classification: ", classification, "Draft Breed: ", draftBreed);
		
		return horseDetails;
	}
}
