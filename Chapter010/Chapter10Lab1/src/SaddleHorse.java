/**
 * 
 */

/**
 * @author RayOsborne
 * @date 12-1-2015
 * @purpose Extend Horse class to include information about Saddle Horse information
 */
public class SaddleHorse extends Horse {
	// Declare variables needed for DraftHorse class
	private String saddleType;
	private String saddleBreed;
	
	// Draft Horse constructor method
	public SaddleHorse () {
		
	}
	
	// Horse overloaded constructor method
	public SaddleHorse (int anIdNo, String aCoatColor, int anAge, double aHeight, String aSaddleType, String aSaddleBreed) {
		// Call super class constructor
		super(anIdNo, aCoatColor, anAge, aHeight);
		
		// Call subclass mutators
		setSaddleType(aSaddleType);
		setSaddleBreed(aSaddleBreed);
	}
	
	// Subclass Mutator methods
	public void setSaddleType(String aSaddleType) {
		saddleType = aSaddleType;
	}
	public void setSaddleBreed(String aSaddleBreed) {
		saddleBreed = aSaddleBreed;
	}
	
	// Subclass Accessor methods
	public String getSaddleType() {
		return saddleType;
	}
	public String getSaddleBreed() {
		return saddleBreed;
	}
	
	// Display Saddle Horse subclass information
	public String tellAboutHorse() {
		// Declare variables needed for tellAboutHorse method
		String horseDetails;
		
		// Build return string
		horseDetails = String.format("%s%n%n%s%n%s%n%n%s%n%ss", super.tellAboutHorse(), "Saddle Type: ", saddleType, "Saddle Breed: ", saddleBreed);
		
		return horseDetails;
	}
}
