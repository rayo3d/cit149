/**
 * 
 */

/**
 * @author RayOsborne
 * @date 12-1-2015
 * @purpose Contain basic attributes for all show horses
 */
public class Horse {
	// Declare variables used in Horse class
	private int idNo;
	private String coatColor;
	private int age;
	private double height;
	
	// Horse constructor method
	public Horse () {
		
	}
	
	// Horse overloaded constructor method
	public Horse (int anIdNo, String aCoatColor, int anAge, double aHeight) {
		// Call mutators to update values
		setIdNo(anIdNo);
		setCoatColor(aCoatColor);
		setAge(anAge);
		setHeight(aHeight);
	}
	
	// Mutator methods
	public void setIdNo(int anIdNo) {
		idNo = anIdNo;
	}
	public void setCoatColor(String aCoatColor) {
		coatColor = aCoatColor;
	}
	public void setAge(int anAge) {
		age = anAge;
	}
	public void setHeight(double aHeight) {
		height = aHeight;
	}
	
	// Accessor methods
	public int getIdNo() {
		return idNo;
	}
	public String getCoatColor() {
		return coatColor;
	}
	public int getAge() {
		return age;
	}
	public double getHeight() {
		return height;
	}
	
	// Display Horse information
	public String tellAboutHorse() {
		// Declare variables needed for tellAboutHorse method
		String horseDetails;
		
		// Build return string
		horseDetails = String.format("%n%n%s%n%d%n%n%s%n%s%n%n%s%n%d%n%n%s%n%.1f%s", "ID Number: ", idNo, "Coat Color: ", coatColor, "Age: ", age, "Height: ", height, " hands ");
		
		return horseDetails;
	}
}
