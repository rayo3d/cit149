/**
 * 
 */

/**
 * @author Ray
 * @date 12-2-2015
 * @purpose Contain basic attributes for all games
 */
public class Game {
	// Declare variables used in Game class
	private int ram;
	private int hardDrive;
	private double cpu;
	
	// Game constructor method
	public Game () {
		
	}
	
	// Game overloaded constructor method
	public Game (int amtRam, int amtHardDrive, double amtCPU) {
		// Call mutators to update values
		setRam(amtRam);
		setHardDrive(amtHardDrive);
		setCpu(amtCPU);
	}
	
	// Mutator methods
	public void setRam(int amtRam) {
		ram = amtRam;
	}
	public void setHardDrive(int amtHardDrive) {
		hardDrive = amtHardDrive;
	}
	public void setCpu(double amtCPU) {
		cpu = amtCPU;
	}
	
	// Accessor methods
	public int getRam() {
		return ram;
	}
	public int getHardDrive() {
		return hardDrive;
	}
	public double getCpu() {
		return cpu;
	}
	
	// Return formated Game info
	public String toString() {
		// Declare variables needed for toString
		String gameInfo;
		
		// Format return string
		gameInfo = "Minimum configuration: RAM: " + ram + "MB; Hard Disk: " + hardDrive + "MB; CPU: " + cpu + "Ghz.\n"; 
		
		return gameInfo;
	}
}
