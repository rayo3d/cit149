/**
 * 
 */

/**
 * @author Ray
 * @date 12-2-2015
 * @purpose Extend Game class to include information about a pc game
 */
public class PCGame extends Game {
	// Declare variables needed for DraftHorse class
	private String gameName;
	
	// PCGame constructor method
	public PCGame () {
		
	}
	
	// PCGame overloaded constructor method
	public PCGame (int amtRam, int amtHardDrive, double Cpu, String aPcGame) {
		// Call super class constructor
		super(amtRam, amtHardDrive, Cpu);
		
		// Call subclass mutators
		setPcGameName(aPcGame);
	}
	
	// Subclass Mutator methods
	public void setPcGameName(String aPcGame) {
		gameName = aPcGame;
	}
	
	// Subclass Accessor methods
	public String getPcGameName() {
		return gameName;
	}
	
	// Return formated PC Game info
	public String toString() {
		// Declare variables needed for toString
		String gameInfo;
		
		// Format return string
		gameInfo = "Description: " + gameName + "\n" + super.toString(); 
		
		return gameInfo;
	}
}
