/**
 * 
 */

/**
 * @author RayOsborne
 * @date 12-2-2015
 * @purpose Test Game class and PCGame subclass and inheritance
 */

import java.util.Scanner;

public class Ch10PA1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed for main method
		Scanner keyboard = new Scanner(System.in);
		PCGame game1 = new PCGame(256, 3000, 1.8, "CounterStrike");
		PCGame game2 = new PCGame(128, 1500, 1.5, "NFL");
		PCGame game3 = new PCGame(512, 2000, 1.5, "Star Wars Galaxies");
		
		// Display Game info
		System.out.println(game1.toString());
		System.out.println(game2.toString());
		
		// Compare games for equality
		if (game1 == game2)
			System.out.println(game1.getPcGameName() + " is equal to " + game2.getPcGameName());
		else
			System.out.println(game1.getPcGameName() + " is not equal to " + game2.getPcGameName());
		
		// Reset game1 and game2
		game1 = game3;
		game2 = game3;
		System.out.println();
		
		// Re-compare games for equality
		if (game1 == game2)
			System.out.println(game1.getPcGameName() + " is equal to " + game2.getPcGameName());
		else
			System.out.println(game1.getPcGameName() + " is not equal to " + game2.getPcGameName());
	}

}
