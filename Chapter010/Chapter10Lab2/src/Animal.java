/**
 * 
 */

/**
 * @author Ray
 * @date 12-1-2015
 * @purpose Create abstract class for demo
 */
public abstract class Animal {
	// Declare variables needed for Animal abstract class
	String type;
	
	// Constructor for Animal class
	public Animal(String type) {
		this.type = new String(type);
	}
	
	// Abstract methods for Animal class
	public abstract void describe();
	public abstract void sound();
	public abstract void sleep();
	public abstract void move();
}
