/**
 * 
 */

/**
 * @author Ray
 * @date 12-1-2015
 * @purpose Test Horse and Dinosaur subclasses and inheritance from Animal abstract class
 */
public class Ch10Lab2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare objects needed for main method
		Horse horse = new Horse("American Pharoah", "Thoroughbred");
		Dinosaur dino = new Dinosaur("Rex", "Tyrannosaurus Rex");
		
		// Display horse info
		System.out.println("For the horse: ");
		System.out.print("This is:  "); 
		horse.describe();
		System.out.print("Sound:    ");
		horse.sound();
		System.out.print("Sleeping: ");
		horse.sleep();
		System.out.print("Moving:   ");
		horse.move();
		System.out.println("\n");
		
		// Display dino info
		System.out.println("For the dinosaur: ");
		System.out.print("This is:  "); 
		dino.describe();
		System.out.print("Sound:    ");
		dino.sound();
		System.out.print("Sleeping: ");
		dino.sleep();
		System.out.print("Moving:   ");
		dino.move();
		System.out.println("\n");
	}

}
