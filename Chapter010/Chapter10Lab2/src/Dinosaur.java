/**
 * 
 */

/**
 * @author Ray
 * @date 12-1-2015
 * @purpose Extend abstract Animal class
 */
public class Dinosaur extends Animal {
	// Declare variables for Horse subclass
	// Name of a Dinosaur
	private String name;
	// Dinosaur breed
	private String breed;
	
	// Constructor for Dinosaur subclass
	public Dinosaur (String aName) {
		// Call the base constructor
		super("Dinosaur");
		// Supplied name
		name = aName;
		// Default breed
		breed = "Unknown";
	}
	
	// Overloaded constructor for Dinosaur subclass
	public Dinosaur (String aName, String aBreed) {
		// Call the base constructor
		super("Dinosaur");
		// Supplied name
		name = aName;
		// Supplied breed
		breed = aBreed;
	}

	/* (non-Javadoc)
	 * @see Animal#describe()
	 */
	@Override
	public void describe() {
		// Output Dinosaur informaion
		System.out.println(name + " the " + breed);
	}

	/* (non-Javadoc)
	 * @see Animal#sound()
	 */
	@Override
	public void sound() {
		// Output sound for Dinosaur
		System.out.println("Roar Roar");
	}

	/* (non-Javadoc)
	 * @see Animal#sleep()
	 */
	@Override
	public void sleep() {
		// Output Dinosaur sleeping style
		System.out.println(name + " sleeps wherever he wants!");
	}

	/* (non-Javadoc)
	 * @see Animal#move()
	 */
	@Override
	public void move() {
		// Output Dinosaur movement type
		System.out.println("Moves very fast!");
	}

	// Accessor method for Dinosaur sublcass
	public String getName() {
		return name;
	}
}
