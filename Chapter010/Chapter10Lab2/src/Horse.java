/**
 * 
 */

/**
 * @author Ray
 * @date 12-1-2015
 * @purpose Extend abstract Animal class
 */
public class Horse extends Animal {
	// Declare variables for Horse subclass
	// Name of a Horse
	private String name;
	// Horse breed
	private String breed;
	
	// Constructor for Horse subclass
	public Horse (String aName) {
		// Call the base constructor
		super("Horse");
		// Supplied name
		name = aName;
		// Default breed
		breed = "Unknown";
	}
	
	// Overloaded constructor for Horse subclass
	public Horse (String aName, String aBreed) {
		// Call the base constructor
		super("Horse");
		// Supplied name
		name = aName;
		// Supplied breed
		breed = aBreed;
	}

	/* (non-Javadoc)
	 * @see Animal#describe()
	 */
	@Override
	public void describe() {
		// Output Horse informaion
		System.out.println(name + " the " + breed);
	}

	/* (non-Javadoc)
	 * @see Animal#sound()
	 */
	@Override
	public void sound() {
		// Output sound for Horse
		System.out.println("Neigh Neigh");
	}

	/* (non-Javadoc)
	 * @see Animal#sleep()
	 */
	@Override
	public void sleep() {
		// Output Horse sleeping style
		System.out.println(name + " sleeps standing up!");
	}

	/* (non-Javadoc)
	 * @see Animal#move()
	 */
	@Override
	public void move() {
		// Output Horse movement type
		System.out.println("Moves very fast!");
	}

	// Accessor method for Horse sublcass
	public String getName() {
		return name;
	}
}
