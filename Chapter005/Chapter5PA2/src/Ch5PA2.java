/*
 * Date: October-17-2015
 * Programmer: Ray Osborne
 * Purpose: Capture user entered web address and determine its originating entity
*/

import java.util.Scanner;

public class Ch5PA2 {

	public static void main(String[] args) {
		// Initialize variables needed in Main method
		int convertYear, yearLength;
		String inputYear;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Ask for user input
		System.out.println("Please enter a year: ");
		inputYear = keyboard.nextLine();
		
		yearLength = inputYear.length();
		
		// Check year length
		switch (yearLength) {
			case 2:
				convertYear = (Integer.parseInt(inputYear)) + 2000;
				System.out.println("You entered " + inputYear + " and it was converted to " + convertYear);
								
				break;
			case 4:
				convertYear = Integer.parseInt(inputYear);
				System.out.println("You entered " + inputYear + " and it was converted to " + convertYear);
				
				break;
			default:
				System.out.println("You have entered an invalid year");
				System.exit(0);
		}
	}

}
