/*
 * Date: October-15-2015
 * Programmer: Ray Osborne
 * Purpose: To check to see if a string contains specific words
*/

import java.util.Scanner;

public class Ch5Lab1 {

	public static void main(String[] args) {
		// Initialize variables needed for Main method
		String line;
		int index1, index2, index3;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Ask for user input
		System.out.println("Please enter a line to be checked to see if it contains either the word 'course', 'school', or 'java': ");
		// Capture input and convert all to lowercase
		line = keyboard.nextLine().toLowerCase();
		
		// Get index location of search words (if available)
		index1 = line.indexOf("course");
		index2 = line.indexOf("school");
		index3 = line.indexOf("java");
		
		// Display user input line
		System.out.println("You entered " + line);
		
		// Displays appropriate output if search words are found
		if (index1 > -1)
			System.out.println("Your line contains the word 'course'.");
		else
			System.out.println("Your line does not contain the word 'course'.");
		
		if (index2 > -1)
			System.out.println("Your line contains the word 'school'.");
		else
			System.out.println("Your line does not contain the word 'school'.");
		
		if (index3 > -1)
			System.out.println("Your line contains the word 'java'.");
		else
			System.out.println("Your line does not contain the word 'java'.");
	}

}
