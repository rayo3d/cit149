/*
 * Date: October-15-2015
 * Programmer: Ray Osborne
 * Purpose: Capture user input temperature in C or F then convert to opposite format
*/

import java.util.Scanner;

public class Ch5Lab3Switch {

	public static void main(String[] args) {
		// Initialize variables needed for Main method
		Scanner keyboard = new Scanner(System.in);
		
		double degrees, converted;
		String scale;
		
		// Capture user input for degrees
		System.out.println();
		System.out.println();
		System.out.println("Enter a temperature in degrees (for exmaple 29.6): ");
		degrees = keyboard.nextDouble();
		
		// Capture user input temperature format
		System.out.println();
		System.out.println("Enter 'f' for Fahrenheit or 'c' for Celsius: ");
		scale = keyboard.next();
		System.out.println();
		
		// Check temperature scale and then perform appropriate conversion
		switch (scale) {
			case "F": case "f":
				converted = 5 * (degrees - 32) / 9;
			
				System.out.println(degrees + " degrees Fahrenheit = " + converted + "degrees Celsius.");				
				break;
			case "C": case "c":
				converted = degrees * 9 / 5 + 32;
			
				System.out.println(degrees + " degrees Celsius = " + converted + "degrees Fahrenheit.");
				break;
			default:
				System.out.println("You have entered an incorrect scale. This program will now end.");
			
				System.exit(0);
		}
	}

}
