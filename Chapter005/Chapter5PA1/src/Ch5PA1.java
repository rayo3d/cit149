/*
 * Date: October-17-2015
 * Programmer: Ray Osborne
 * Purpose: Caputer user entered web address and determine its originating entity
*/

import java.util.Scanner;

public class Ch5PA1 {

	public static void main(String[] args) {
		// Initialize variables needed for Main method
		String webAddress;
		int indexGov, indexEdu, indexCom, indexOrg;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Ask for user input
		System.out.println("Please enter a web addess (Example: www.yahoo.com): ");
		// Capture input and convert all to lowercase
		webAddress = keyboard.nextLine().toLowerCase();
		
		// Spacing before results
		System.out.println();
		System.out.println();
		
		// Get index location of search words (if available)
		indexGov = webAddress.indexOf("gov");
		indexEdu = webAddress.indexOf("edu");
		indexCom = webAddress.indexOf("com");
		indexOrg = webAddress.indexOf("org");
		
		// Displays appropriate output if search words are found
		if (indexGov > -1)
			System.out.println("The website is a government web address.");
		else if (indexEdu > -1)
			System.out.println("The website is a university web address.");
		else if (indexCom > -1)
			System.out.println("The website is a business web address.");
		else if (indexOrg > -1)
			System.out.println("The website is a organization web address.");
		else
			System.out.println("The website is for another organization.");
	}

}
