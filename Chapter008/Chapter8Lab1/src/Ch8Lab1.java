/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-16-2015
 * @purpose Determine if a car or cars need maintenance based on the miles driven and how many months since the car last received general maintenance
 */

import java.util.Scanner;

public class Ch8Lab1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed by Main method
		Scanner keyboard = new Scanner(System.in);
		int size;
		
		// Ask user for number of cars to be entered
		System.out.println("How many cars will you enter?");
		size = keyboard.nextInt();
		
		// Declare and construct arrays
		int[] stockNumber = new int[size];
		String[] autoVenNumber = new String[size];
		double[] milesDriven = new double[size];
		double[] lastServiced = new double[size];
		double[] excessMilesDriven = new double[size];
		double[] excessMonths = new double[size];
		final double MILE_LIMIT = 10000;
		final double MONTH_LIMIT = 6;
		
		// Loop to capture user input for each array item
		for (int i = 0; i < size; i++) {
			// Capture input
			System.out.print("What is the stock number of car #" + (i + 1) + ": ");
			stockNumber[i] = keyboard.nextInt();
			
			System.out.print("What is the vendor number of car #" + (i + 1) + ": ");
			autoVenNumber[i] = keyboard.next();
			
			System.out.print("How many months has it been since car #" + (i + 1) + " was last serviced: ");
			lastServiced[i] = keyboard.nextDouble();
			
			System.out.print("What are the number of miles driven since car #" + (i + 1) + " was last serviced: ");
			milesDriven[i] = keyboard.nextDouble();
			
			// Check if miles driven is more than the limit
			if (milesDriven[i] > MILE_LIMIT) {
				excessMilesDriven[i] = milesDriven[i] - MILE_LIMIT;
			} else {
				excessMilesDriven[i] = 0;
			}
			
			// Check if last serviced time is over month limit
			if (lastServiced[i] > MONTH_LIMIT) {
				excessMonths[i] = lastServiced[i] - MONTH_LIMIT;
			} else {
				excessMonths[i] = 0;
			}
		}
		
		// Display result detailing if car requires maintenance
		for (int i = 0; i < size; i++) {
			if ((excessMilesDriven[i] > 100) && (excessMonths[i] == 0)) {
				System.out.println("Car " + stockNumber[i] + " Maintenance Schedule A");
			} else if ((excessMilesDriven[i] > 100) && (excessMonths[i] > 0)) {
				System.out.println("Car " + stockNumber[i] + " Maintenance Schedule B");
			} else {
				System.out.println("Car " + stockNumber[i] + " Needs no maintenance at this time");
			}
		}
	}

}
