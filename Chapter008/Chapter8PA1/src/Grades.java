/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-19-2015
 * @purpose Encapsulate the concept of student test grades from a class
 */

import java.util.Random;

public class Grades {
	// Declare variables needed in Grades class
	private int[] grades = new int[11];
	private int[] testGrades = new int[11];
	private int highestGrade;
	private double avgGrade;
	private int medianGrade;
	private int modeGrade;
	
	// Constructors for Grades class
	public Grades(int[] studentGrades) {
		// Assign class Variables values
		grades = studentGrades;
		highestGrade = getHighestGrade();
		avgGrade = getAvgGrade();
		medianGrade = getMedianGrade();
		modeGrade = getModeGrade();
	}
	
	public Grades() {
	}
	
	// Randomly generate grades for tests
	public int[] getTestGrades() {
		// Random variables for random generator
		Random rand = new Random();
		
		// Randomly generate grades and populate array
		for (int c = 0; c < grades.length; c++) {
			grades[c] = rand.nextInt((100 - 0) + 1) + 0;
		}
		
		return grades;
	}
	
	// Find average grades
	public double getAvgGrade() {
		// Set variables needed for getAvgGrade method
		int totalGrades = 0;
		double gradesAvg = 0;
		
		// Add all grades together
		for (int c = 0; c < grades.length; c++) {
			totalGrades += grades[c];
		}
		
		gradesAvg = totalGrades / grades.length;
		
		return gradesAvg;
	}
	
	// Find median grade
	public int getModeGrade() {
		// Set variables needed for getModeGrade method
		int gradeKey = 0;
	    int gradeCounts = 0;

	    for (int i = 0; i < grades.length; ++i) {
	        int count = 0;
	        for (int j = 0; j < grades.length; ++j) {
	            if (grades[j] == grades[i]) ++count;
	        }
	        if (count > gradeCounts) {
	        	gradeCounts = count;
	        	gradeKey = grades[i];
	        }
	    }
	    
	    return gradeKey;
	}
	
	// Get the highest grade
	public int getHighestGrade() {
		// Declare variables needed for getHighestGrade method
		int highGrade = 0;
		
		// Get highest grade
		highGrade = grades[grades.length - 1];
		
		return highGrade;
	}
	
	// Find median grade
	public int getMedianGrade() {
		// Set variables needed for getMedianGrade method
		int medGrade = 0;
				
		medGrade = grades[grades.length / 2];
		
		return medGrade;
	}
	
	// Sort the grades array
	public int[] sortGrades() {
		// Declare variables needed for sortGrades method
		int temp = 0;
		
		// Check grades array values and sort array
		for (int i = 0; i < grades.length; i++) {
			for (int j = 0; j < grades.length - 1; j ++) {
				if (grades[j] > grades[j + 1]) {
					// Swap array placement
					temp = grades[j];
					grades[j] = grades[j + 1];
					grades[j + 1] = temp;
				}
			}
		}
		
		return grades;
	}
	
	// Display grades as a string
	public String toString() {
		// Declare variables needed in toString method
		String returnString = "Grades: ";
		
		// Loop to add grades in array to returnString
		for (int i = 0; i < grades.length; i++) {
			returnString += (grades[i] + " ");
		}
		
		return returnString;
	}
	
	// Check equality between two objects
	/* equals
	 * @param o another Grades object
	 * @return return true if elements of array in g2 are equal to corresponding elements in this object and arrays are the same length
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Grades))
			return false;
		else {
			Grades g2 = (Grades) o;
			
			if (grades.length != g2.grades.length)
				return false;
			for (int i = 0; i < grades.length; i++) {
				if (grades[i] != g2.grades[i])
					return false;
			}
			
			return false;
		}
	}
}
