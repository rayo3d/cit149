/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-19-2015
 * @purpose Test methods in the Grades class
 */

import java.text.DecimalFormat;

public class Ch8PA1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables used in main method
		Grades g1 = new Grades();
		Grades g2 = new Grades();
		Grades studentGrades = new Grades();		
		
		DecimalFormat df = new DecimalFormat("#0.00");
		
		// Generate grades
		g1.getTestGrades();
		g2.getTestGrades();
		studentGrades.getTestGrades();
		
		// Display formated grades with toString
		System.out.println("g1:\n" + g1);
		System.out.println("g2:\n" + g2);
		
		// Check if grade arrays are equal
		if (g1.equals(g2))
			System.out.println("Objects are equal\n");
		else
			System.out.println("Objects are not equal\n");
		
		// Set temps arrays to be equal and check equals again
		System.out.println("Setting g1 and g2 arrays to entered studentGrades");
		g1 = studentGrades;
		g2 = studentGrades;
		
		// Displaying new values for both objects
		System.out.println(studentGrades);
		
		System.out.println("Comparing g1 and g2 for equality");
		
		if (g1.equals(g2))
			System.out.println("Objects are equal\n");
		else
			System.out.println("Objects are not equal\n");
		
		// Display sorted studentGrades
		studentGrades.sortGrades();
		System.out.println("Sorted Grades:\n" + studentGrades);
		
		// Display highest grade
		System.out.println("\nThe highest grade is " + studentGrades.getHighestGrade());
		
		// Display average grade
		System.out.println("The average grade is " + df.format(studentGrades.getAvgGrade()));
		
		// Display median grade
		System.out.println("The median grade is " + studentGrades.getMedianGrade());
		
		// Display mode grade
		System.out.println("The mode is " + studentGrades.getModeGrade());
	}

}
