/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-17-2015
 * @purpose Encapsulate the concept of daily temperatures for a week
 */
public class Temperature {
	// Declare variables needed by Temperature method
	private int[] temperatures = new int[7];
	private int freezing;
	private int above;
	private int[] above100Array;
	private int highestChange;
	private int[] temp = new int[7];
	
	// Constructors for Temperature class
	public Temperature(int[] temps) {
		// Assign class Variables values
		temperatures = temps;
		above = getCountAbove();
		above100Array = new int[above];
		above100Array = temperaturesAbove100();
	}
	
	public Temperature() {
	}
	
	// Create temperatures to be used for testing
	public int[] getTestTemperatures() {
		// Assign test values to array
		temp[0] = 97;
		temp[1] = 66;
		temp[2] = 88;
		temp[3] = 89;
		temp[4] = 90;
		temp[5] = 91;
		temp[6] = 77;
		
		temperatures = temp;
		
		return temperatures;
	}
	
	// Track temperatures above 100
	public int getCountAbove() {
		// reset above count for new check
		above = 0;
		
		// Count temperatures over 100
		for (int i = 0; i < temperatures.length; i++) {
			if (temperatures[i] > 100)
				above++;
		}
		
		return above;
	}
	
	// Track temperatures below freezing
	public int belowFreezing() {
		// Count temperatures under 33
		for (int i = 0; i < temperatures.length; i++) {
			if (temperatures[i] < 33)
				freezing++;
		}
		
		return freezing;
	}
	
	// Add temperatures above 100 to above100Array
	public int[] temperaturesAbove100() {
		// reset count for new check
		int count = 0;
		int j = 0;
		
		// Count temperatures over 100
		for (int i = 0; i < temperatures.length; i++) {
			if (temperatures[i] > 100) {
				above100Array[count] = temperatures[i];
				count++;
			}
		}
		
		return above100Array;
	}
	
	// Check for two highest temperatures is highest among consecutive temperatures
	public int largestChange() {
		// Declare variables needed in largestChange method
		int highestChange = 0;
		
		// Loop through temperatures to check for highest change
		for (int i = 1; i < temperatures.length; i++) {
			if (temperatures[i] > temperatures[i - 1]) {
				if ((temperatures[i] - temperatures[i - 1]) > highestChange) {
					highestChange = temperatures[i] - temperatures[i - 1];
				}
			} else if (temperatures[i - 1] > temperatures[i]) {
				if ((temperatures[i - 1] - temperatures[i]) > highestChange) {
					highestChange = temperatures[i - 1] - temperatures[i];
				}
			}
		}
		
		return highestChange;
	}
	
	// Sort the temperatures array
	public void sortArray() {
		// Declare variables needed for sortArray method
		int temp = 0;
		
		// Check temperature array values and sort array
		for (int i = 0; i < temperatures.length; i++) {
			for (int j = 0; j < temperatures.length - 1; j ++) {
				if (temperatures[j] > temperatures[j + 1]) {
					// Swap array placement
					temp = temperatures[j];
					temperatures[j] = temperatures[j + 1];
					temperatures[j + 1] = temp;
				}
			}
		}
	}
	
	// Display temperatures in correct form
	public String toString() {
		// Declare variables needed in toString method
		String returnString = "Temperatures: ";
		
		// Loop to add temperatures in array to returnString
		for (int i = 0; i < temperatures.length; i++) {
			returnString += temperatures[i] + " ";
		}
		
		return returnString;
	}
	
	// Check equality between two objects
	/* equals
	 * @param o another Temperatures object
	 * @return return true if elements of array in g2 are equal to corresponding elements in this object and arrays are the same length
	 */
	public boolean equals(Object o) {
		if (!(o instanceof Temperature))
			return false;
		else {
			Temperature g2 = (Temperature) o;
			
			if (temperatures.length != g2.temperatures.length)
				return false;
			for (int i = 0; i < temperatures.length; i++) {
				if (temperatures[i] != g2.temperatures[i])
					return false;
			}
			
			return false;
		}
	}
}
