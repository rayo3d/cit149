/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-17-2015
 * @purpose Test methods in the Temperature class
 */

import java.util.Scanner;

public class Ch8Lab3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables used in main method
		int[] temperatures1 = new int[7];
		int[] temperatures2;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Request and capture week's worth of temperatures
		System.out.println("\nEnter this week's temperatures\n");
		
		// Loop to capture temperatures
		for (int i = 0; i < temperatures1.length; i++) {
			temperatures1[i] = keyboard.nextInt();
		}
		
		Temperature temps1 = new Temperature(temperatures1);
		
		// Call toString to format output
		System.out.println("\ntemps1:\n" + temps1 + "\n");
		
		// Find the highest temperature between any two consecutive days in temps1 and display it
		System.out.println("\nThe highest temperature for temps1 between any two consecutive days is: " + temps1.largestChange() + " degress\n");
		
		// Display how many of the entered temperatures in temps1 are below freezing
		System.out.println("\nTemperatures entered below 33 degrees for temps1: " + temps1.belowFreezing() + "\n");
		
		// Check temps1.getCountAbove() is greater than 0 and display
		if (temps1.getCountAbove() > 0) {
			int[] above100 = temps1.temperaturesAbove100();
			
			// Display temperatures above 100
			System.out.println("Temperatures above 100 for temps1 are : \n");
			
			for (int i = 0; i < above100.length; i++) {
				System.out.print(above100[i] + " ");
				System.out.println();
			}
		}
		
		// Sort array and display updated version of temps1 array
		temps1.sortArray();
		System.out.println("\nSorted temperatures for temps1 are\n" + temps1 + "\n");
		
		// Set the second array to equal what is returned by the getTestTemperatures() method
		// Create a new Temperature object based on the test temperatures
		Temperature temps2 = new Temperature();
		temperatures2 = temps2.getTestTemperatures();
		
		// Display formated temps2 with toString
		System.out.println("\ntemps2:\n" + temps2 + "\n");
		
		// Find the highest temperature between any two consecutive days in temps2 and display it
		System.out.println("\nThe highest temperature for temps2 between any two consecutive days is: " + temps2.largestChange() + " degress\n");
		
		// Display how many of the entered temperatures in temps2 are below freezing
		System.out.println("\nTemperatures entered below 33 degrees for temps2: " + temps2.belowFreezing() + "\n");
		
		// Check temps2.getCountAbove() is greater than 0 and display
		if (temps2.getCountAbove() > 0) {
			int[] above100 = temps2.temperaturesAbove100();
			
			// Display temperatures above 100
			System.out.println("Temperatures above 100 for temps2 are : \n");
			
			for (int i = 0; i < above100.length; i++) {
				System.out.print(above100[i] + " ");
				System.out.println();
			}
		} else
			System.out.println("There are no temperatures above 100");
		
		// Sort array and display updated version of temps2 array
		temps2.sortArray();
		System.out.println("\nSorted temperatures for temps2 are\n" + temps2 + "\n");
		
		// Test equals between temps objects
		System.out.println("\nTesting to see if the two objects are equals.");
		
		if (temps1.equals(temps2))
			System.out.println("\nObjects are equal\n");
		else
			System.out.println("\nObjects are not equal\n");
		
		// Set temps arrays to be equal and check equals again
		System.out.println("\nSetting temps1 and temps2 arrays to entered temperatures\n");
		temps2 = temps1;
		
		// Displaying new values for both objects
		System.out.println("temps:\n" + temps1);
		System.out.println("temps:\n" + temps2);
		
		System.out.println("\nTesting to see if the two objects are equals\n");
		
		if (temps1.equals(temps2))
			System.out.println("\nObjects are equal\n");
		else
			System.out.println("\nObjects are not equal\n");
	}

}
