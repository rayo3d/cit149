
public class Ch2Lab2 {

	public static void main(String[] args) {
		// Find the area of a Parallelogram
		double area;
		double base = 2.5;
		double height = 5.5;
		
		area = base * height;
		
		System.out.println("The Parallelogram:\nBase: " + base + " inches\nHeight: " + height + " inches\nArea: " + area + " inches.");
	}

}
