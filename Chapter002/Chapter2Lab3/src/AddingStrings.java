/*
 * Program: AddingStrings.java
 * Programmer: Raymond Osborne
 * Purpose: Accept user input and add two strings together.
 */

import java.util.Scanner;

public class AddingStrings {

	public static void main(String[] args) {
		// Declare variables for this class
		String first, last, fullName, major;
		Scanner keyboard= new Scanner(System.in);
		
		// Display message to user and capture input
		System.out.println();
		System.out.println("I will add two strings into one statement");
		
		System.out.println();
		System.out.println("Enter your first name");
		first = keyboard.nextLine();
		
		System.out.println();
		System.out.println("Enter your last name and hit Enter");
		last = keyboard.nextLine();
		
		System.out.println();
		System.out.println("What is your major?");
		major = keyboard.nextLine();
		
		// Combine first and last name variables
		fullName = first + " " + last;
		
		// Display final version of name with major
		System.out.println();
		System.out.println("Full Name: " + fullName);
		System.out.println();
		System.out.println("Major in: " + major + "\n");
	}

}
