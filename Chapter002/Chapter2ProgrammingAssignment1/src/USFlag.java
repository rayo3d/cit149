/*
 * Program: USFlag.java
 * Programmer: Raymond Osborne
 * Purpose: Display US Flag using asterisks (*).
 */

public class USFlag {

	public static void main(String[] args) {
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println(" *     *     *     *     *     *     ***************************************");
		System.out.println("    *     *     *     *     *                                              *");
		System.out.println(" *     *     *     *     *     *     ***************************************");
		System.out.println("    *     *     *     *     *                                              *");
		System.out.println(" *     *     *     *     *     *     ***************************************");
		System.out.println("    *     *     *     *     *                                              *");
		System.out.println(" *     *     *     *     *     *     ***************************************");
		System.out.println("    *     *     *     *     *                                              *");
		System.out.println(" *     *     *     *     *     *     ***************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
		System.out.println("                                                                           *");
		System.out.println("****************************************************************************");
	}

}
