/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-28-2015
 * @purpose Display the month of November 2015 using multi-dimensional arrays
 */
public class Ch9Lab1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and arrays needed for Main method
		String[][] calendar = new String[7][7];
		int row, column, j = 1;
		
		// Set first row of array
		calendar[0][0] = " S";
		calendar[0][1] = " M";
		calendar[0][2] = " T";
		calendar[0][3] = " W";
		calendar[0][4] = " R";
		calendar[0][5] = " F";
		calendar[0][6] = " S";
		
		// Set days of the month
		for (row = 1; row < 7; row++) {
			for (column = 0; column < 7; column++) {
				if (j > 0 && j <= 9)
					calendar[row][column] = " " + String.valueOf(j);
				else if (j > 9 && j <= 30) {
					calendar[row][column] = String.valueOf(j);
				} else 
					calendar[row][column] = " ";
				
				j++;
			}
		}
		
		// Display the Calendar
		System.out.println(" November 2015 Calendar");
		for (row = 0; row < calendar.length; row++) {
			for (column = 0; column < calendar[row].length; column++) {
				System.out.print(calendar[row][column] + " ");
			}
			System.out.println();
		}
	}

}
