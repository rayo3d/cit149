/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-29-2015
 * @purpose Create a Sudoku 16x16 puzzle game
 */

import java.util.Scanner;

public class SudokuPuzzle {
	// Declare variables and arrays needed for SudokuPuzzle class
	private int board[][];
	private int start[][];
	static int rows = 16;
	static int cols = 16;
	static int vals = 16;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables needed for Main method
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Sudoku Game: ");
		
		// Declare new SudokuPuzzle object
		SudokuPuzzle puzzle = new SudokuPuzzle();
		puzzle.initializePuzzle(puzzle);
		
		// Display puzzle
		System.out.print("The puzzle is: \n" + puzzle);
		
		// Check user input for desired action
		boolean done = false;
		
		while (!done) {
			System.out.println("What would you like to do?\n" + "Clear Puzzle (C), Set a square (S), Get possible values (G), Quit (Q)");
			String response = reader.next();
			response = response.toLowerCase();
			
			if (response.equals("q")) {
				// Quit game chosen
				System.out.println("Thanks for playing.");
				
				done = true;
			} else if (response.equals("s")) {
				// Set square chosen
				// Choose cell to change
				System.out.println("Which row (1-" + rows + ") and column (1-" + cols + ") do you want to change?");
				int row = reader.nextInt() - 1;
				int col = reader.nextInt() - 1;
				
				// Change cell
				System.out.println("What should the value (1-" + vals + ") be?");
				int value = reader.nextInt();
				
				// Update cell
				puzzle.addGuess(row, col, value);
			} else if (response.equals("g")) {
				// Get possible values chosen
				// Choose cell to check
				System.out.println("Which row (1-" + rows + ") and column (1-" + cols + ") do you want to get values for?");
				int row = reader.nextInt() - 1;
				int col = reader.nextInt() - 1;
				
				// Return values
				boolean valid[] = puzzle.getAllowValues(row, col);
				System.out.print("Allowed values are: ");
				
				for (int i = 0; i < vals; i++) {
					if (valid[i])
						System.out.print((i + 1) + " ");
				}
				
				System.out.println();
			} else if (response.equals("c")) {
				// Reset chosen
				puzzle.reset();
			}
			
			// Display updated puzzle
			System.out.print("The puzzle is now:\n" + puzzle);
			
			// Display error message if needed
			if (!puzzle.checkPuzzle())
				System.out.println("You have made an error in the puzzle.");
			else if (puzzle.isFull())
				System.out.println("Congratulations, you have completed the puzzle.");
		}
	}

	// Create SudokuPuzzle constructor class
	public SudokuPuzzle() {
		// Declare variables and arrays needed for SudokuPuzzle method
		start = new int[rows][cols];
		board = new int [rows][cols];
	}
	
	// Set string to display the board
	public String toString() {
		// Declare variables needed for toString method
		String puzzleString = "Ros/Col\n     1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16\n";
		
		puzzleString = puzzleString + "   -------------------------------------------------\n";
		
		// Create board borders
		for (int i = 0; i < rows; i++) {
			if (i < 9)
				puzzleString = puzzleString + (i + 1) + "  |";
			else
				puzzleString = puzzleString + (i + 1) + " |";
			
			for (int j = 0; j < cols; j++) {
				if (board[i][j] == 0)
					puzzleString = puzzleString + " " + ".|";
				else {
					if (board[i][j] <= 9)
						puzzleString = puzzleString + " " + board[i][j] + "|";
					else
						puzzleString = puzzleString + board[i][j] + "|";
				}
			}
			
			puzzleString = puzzleString + "\n";
			puzzleString = puzzleString + "   |__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|__|\n";
		}
		
		return puzzleString;
	}
	
	// Add initial values for the board
	public void addInitial(int row, int col, int value) {
		// If values are valid, populate cell in board
		if (row >= 0 && row <= rows && col >= 0 && col <= cols && value >=1 && value <= vals) {
			start[row][col] = value;
			board[row][col] = value;
		}
	}
	
	// Add guess to a column
	public void addGuess(int row, int col, int value) {
		// Only set the value if the start is 0
		if (row >= 0 && row <= rows && col >= 0 && col <= cols && value >=1 && value <= vals && start[row][col] == 0) {
			board[row][col] = value;
		}
	}
	
	// Return value of particular row or column
	public int getValueIn(int row, int col) {
		return board[row][col];
	}
	
	// Reset the board to the start array
	public void reset() {
		for (int i = 0; i < rows; i++) 
			for (int j = 0; j < cols; j++) 
				board[i][j] = start[i][j];
	}
	
	// Check if all columns and rows are full and return true or false
	public boolean isFull() {
		// Declare variables needed for isFull method
		boolean allFilled = true;
		
		// Check if all are filled
		for (int i = 0; i < rows; i++) 
			for (int j = 0; j < cols; j++) 
				allFilled = allFilled && board[i][j] > 0;
				
		return allFilled;
	}
	
	// Check for allowed values
	public boolean[] getAllowValues(int row, int col) {
		// Save the value at the location, then try all 9 values
		int savedValue = board[row][col];
		
		// Declare variables needed for getAllowValues method
		boolean result[] = new boolean[vals];
		
		// Check value
		for (int value = 1; value <= vals; value++) {
			board[row][col] = value;
			result[value - 1] = checkPuzzle();
		}
		
		// Return saved value to cell
		board[row][col] = savedValue;
		
		return result;
	}
	
	// Check if value in square is legal or not
	public boolean checkPuzzle() {
		// Declare variables checkPuzzle method
		boolean looksGood = true;
		
		// Check rows, columns, and grid around cell
		for (int i = 0; i < vals; i++) {
			looksGood = looksGood && checkRow(i);
			looksGood = looksGood && checkCol(i);
			looksGood = looksGood && checkSub(i);
		}
		
		return looksGood;
	}
	
	// Check row to make sure number does not appear twice in the row
	public boolean checkRow(int row) {
		// Declare variables checkRow method
		int count[] = new int[rows + 1];
		
		// Get count in column
		for (int col = 0; col < rows; col++) {
			count[board[row][col]]++;
		}
		
		// Declare variables checkRow method
		boolean countIsOk = true;
		
		// Check row for duplicate numbers
		for (int i = 1; i <= rows; i++)
			countIsOk = countIsOk && (count[i] <= 1);
		
		return countIsOk;
	}
	
	// Check row to make sure number does not appear twice in the row
	public boolean checkCol(int col) {
		// Declare variables checkCol method
		int count[] = new int[cols + 1];
		
		// Get count in column
		for (int row = 0; row < cols; row++) {
			count[board[row][col]]++;
		}
		
		// Declare variables checkCol method
		boolean countIsOk = true;
		
		// Check column for duplicate numbers
		for (int i = 1; i <= cols; i++)
			countIsOk = countIsOk && (count[i] <= 1);
		
		return countIsOk;
	}
	
	// Checks the 3x3 grid to make sure a number only appears once
	public boolean checkSub(int sub) {
		// Declare variables checkSub method
		int count[] = new int[(vals + 1)];
		// This will give 0, 3, or 6 because of integer division
		int rowBase = (sub / 4) * 4;
		int colBase = (sub % 4) * 4;
		
		// Get count for 3x3 grid
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				count[board[rowBase + i][colBase + j]]++;
			}
		}
		
		// Declare variables checkSub method
		boolean countIsOk = true;
		
		// Check 3x3 grid for duplicate numbers
		for (int i = 1; i <= vals; i++)
			countIsOk = countIsOk && (count[i] <= 1);
				
		return countIsOk;
	}
	
	// Receives an argument of a new SudokuPuzzle object and passes values for each col for the object
	public static void initializePuzzle(SudokuPuzzle p) {
		p.addInitial(0, 4, 12);
		p.addInitial(0, 7, 11);
		p.addInitial(0, 10, 7);
		p.addInitial(0, 11, 5);
		p.addInitial(0, 12, 4);
		
		p.addInitial(1, 5, 2);
		p.addInitial(1, 6, 13);
		p.addInitial(1, 7, 10);
		p.addInitial(1, 13, 15);
		p.addInitial(1, 14, 9);
		p.addInitial(1, 15, 3);
		
		p.addInitial(2, 2, 5);
		p.addInitial(2, 3, 2);
		p.addInitial(2, 4, 6);
		p.addInitial(2, 6, 1);
		p.addInitial(2, 7, 9);
		p.addInitial(2, 9, 10);
		p.addInitial(2, 10, 13);
		p.addInitial(2, 12, 11);
		p.addInitial(2, 13, 7);
		p.addInitial(2, 15, 12);
		
		p.addInitial(3, 0, 6);
		p.addInitial(3, 1, 11);
		p.addInitial(3, 3, 13);
		p.addInitial(3, 4, 15);
		p.addInitial(3, 5, 3);
		p.addInitial(3, 10, 8);
		p.addInitial(3, 12, 16);
		
		p.addInitial(4, 3, 16);
		p.addInitial(4, 9, 7);
		p.addInitial(4, 11, 10);
		p.addInitial(4, 13, 12);
		p.addInitial(4, 14, 6);
		
		p.addInitial(5, 1, 6);
		p.addInitial(5, 3, 8);
		p.addInitial(5, 5, 11);
		p.addInitial(5, 6, 7);
		p.addInitial(5, 7, 15);
		p.addInitial(5, 9, 4);
		p.addInitial(5, 12, 2);
		p.addInitial(5, 13, 5);
		p.addInitial(5, 14, 16);
		
		p.addInitial(6, 0, 9);
		p.addInitial(6, 2, 4);
		p.addInitial(6, 6, 8);
		p.addInitial(6, 9, 16);
		p.addInitial(6, 15, 7);
		
		p.addInitial(7, 2, 11);
		p.addInitial(7, 5, 6);
		p.addInitial(7, 6, 5);
		p.addInitial(7, 10, 2);
		p.addInitial(7, 11, 12);
		p.addInitial(7, 14, 4);
		
		p.addInitial(8, 1, 15);
		p.addInitial(8, 4, 14);
		p.addInitial(8, 5, 9);
		p.addInitial(8, 9, 5);
		p.addInitial(8, 10, 16);
		p.addInitial(8, 13, 6);
		
		p.addInitial(9, 0, 16);
		p.addInitial(9, 6, 15);
		p.addInitial(9, 9, 14);
		p.addInitial(9, 13, 9);
		p.addInitial(9, 15, 8);
		
		p.addInitial(10, 1, 14);
		p.addInitial(10, 2, 7);
		p.addInitial(10, 3, 6);
		p.addInitial(10, 6, 3);
		p.addInitial(10, 8, 2);
		p.addInitial(10, 9, 11);
		p.addInitial(10, 10, 9);
		p.addInitial(10, 12, 12);
		p.addInitial(10, 14, 1);
		
		p.addInitial(11, 1, 12);
		p.addInitial(11, 2, 8);
		p.addInitial(11, 4, 16);
		p.addInitial(11, 6, 10);
		p.addInitial(11, 12, 14);
		
		p.addInitial(12, 3, 11);
		p.addInitial(12, 5, 12);
		p.addInitial(12, 10, 10);
		p.addInitial(12, 11, 3);
		p.addInitial(12, 12, 9);
		p.addInitial(12, 14, 2);
		p.addInitial(12, 15, 1);
		
		p.addInitial(13, 0, 10);
		p.addInitial(13, 2, 9);
		p.addInitial(13, 3, 4);
		p.addInitial(13, 5, 13);
		p.addInitial(13, 6, 6);
		p.addInitial(13, 8, 11);
		p.addInitial(13, 9, 15);
		p.addInitial(13, 11, 1);
		p.addInitial(13, 12, 7);
		p.addInitial(13, 13, 3);
		
		p.addInitial(14, 0, 8);
		p.addInitial(14, 1, 5);
		p.addInitial(14, 2, 1);
		p.addInitial(14, 8, 4);
		p.addInitial(14, 9, 9);
		p.addInitial(14, 10, 12);
		
		p.addInitial(15, 3, 12);
		p.addInitial(15, 4, 9);
		p.addInitial(15, 5, 15);
		p.addInitial(15, 8, 14);
		p.addInitial(15, 11, 13);
	}
}
