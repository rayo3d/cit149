/*
 * Program: Ch3Lab2.java
 * Programmer: Ray Osborne
 * Purpose: Test default and overloaded SaddleHorse class
 */

import java.util.Scanner;

public class Ch3Lab2 {

	public static void main(String[] args) {
		// Declare variables for use in Main method
		Scanner keyboard = new Scanner(System.in);
		
		SaddleHorse horse = new SaddleHorse();
		
		int idNo, age, height;
		String color, saddleType, breed;
		
		// Capture user input for variables needed by the program
		System.out.println("Let's enter a Saddle Horse");
		
		System.out.println("Enter the horse's id number (whole numbers only)");
		idNo = keyboard.nextInt();
		
		System.out.println("Enter the horse's coat color e.g. Roan");
		color = keyboard.next();
		
		System.out.println("Enter the horse's age (whole numbers only)");
		age = keyboard.nextInt();
		
		System.out.println("Enter the height of the horse in hands (whole numbers only)");
		height = keyboard.nextInt();
		
		System.out.println("Enter the saddle type for this horse (English/Western)");
		saddleType = keyboard.next();
		
		keyboard.nextLine();
		
		System.out.println("Enter the breed of the horse e.g. Thoroughbred");
		breed = keyboard.nextLine();
		
		// Pass information to mutator methods in SaddleHorse class
		horse.setIdNo(idNo);
		horse.setCoatColor(color);
		horse.setAge(age);
		horse.setHeight(height);
		horse.setSaddleType(saddleType);
		horse.setSaddleBreed(breed);
		
		// Display information about this horse to test accessor methods in SaddleHorse class
		System.out.println("\t\tSaddle Horse Information\n");
		System.out.println("ID No:\t\t" + horse.getIdNo());
		System.out.println("Coat Color:\t\t" + horse.getCoatColor());
		System.out.println("Age:\t\t" + horse.getAge());
		System.out.println("Height:\t\t" + horse.getHeight());
		System.out.println("Saddle Type:\t" + horse.getSaddleType());
		System.out.println("Breed:\t" + horse.getSaddleBreed());
		
		// Use overload SaddleHorse constructor method
		System.out.println("\n\nLet's create another horse");
		
		SaddleHorse horse2 = new SaddleHorse(3, "Chestnut", 10, 15.5, "English", "Quarter Horse");
		
		// Display information about horse2
		System.out.println("\n\nLet's display the information on the second horse\n");
		System.out.println("\t\tSaddle Horse Information\n");
		System.out.println(horse2.tellAboutHorse());
	}

}
