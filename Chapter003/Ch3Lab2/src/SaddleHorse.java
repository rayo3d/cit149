
public class SaddleHorse {
	// Declare private variables for this class
	private int idNo;
	private String coatColor;
	private int age;
	private double height;
	private String saddleType;
	private String saddleBreed;
	
	// Default SaddleHorse constructor
	public SaddleHorse() {
		
	}
	
	// Overloaded SaddleHorse constructor
	public SaddleHorse(int aIdNo, String aCoatColor, int aAge, double aHeight, String aSaddleType, String aSaddleBreed) {
		idNo = aIdNo;
		coatColor = aCoatColor;
		age = aAge;
		height = aHeight;
		saddleType = aSaddleType;
		saddleBreed = aSaddleBreed;
	}
	
	// Get accessor methods
	public int getIdNo() {
		return idNo;
	}
	public String getCoatColor() {
		return coatColor;
	}
	public int getAge() {
		return age;
	}
	public double getHeight() {
		return height;
	}
	public String getSaddleType() {
		return saddleType;
	}
	public String getSaddleBreed() {
		return saddleBreed;
	}
	
	// Set mutator methods
	public void setIdNo(int anIdNo) {
		idNo = anIdNo;
	}
	public void setCoatColor(String aCoatColor) {
		coatColor = aCoatColor;
	}
	public void setAge(int anAge) {
		age = anAge;
	}
	public void setHeight(double aHeight) {
		height = aHeight;
	}
	public void setSaddleType(String aSaddleType) {
		saddleType = aSaddleType;
	}
	public void setSaddleBreed(String aSaddleBreed) {
		saddleBreed = aSaddleBreed;
	}
	
	// Return information about the Saddle horse
	public String tellAboutHorse() {
		// Returns values of attributes as one string
		String horseDetails;
		
		horseDetails = "ID Number: \t" + idNo + "\nCoat Color:\t" + coatColor + "\nAge:\t" + age + "\nHeight:\t" + height + " hands" + "\nSaddle Type:\t" + saddleType + "\nSaddle Breed:\t" + saddleBreed;
		
		return horseDetails;
	}
}
