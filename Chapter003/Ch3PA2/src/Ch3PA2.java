/*
 * Program: Ch3PA2.java
 * Programmer: Ray Osborne
 * Purpose: This program will get input from user of investment amount and interest rate then display future values after 5, 10, and 20 years.
 */

import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Ch3PA2 {

	public static void main(String[] args) {
		// Variables used in Main method
		Scanner keyboard = new Scanner(System.in);
		double investAmount, interestRate, year5, year10, year20;
		DecimalFormat decimal = new DecimalFormat("0.00%");
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		
		// Capture user investment amount and interst rate
		System.out.print("Enter the investment > ");
		investAmount = keyboard.nextDouble();
		keyboard.nextLine();
		System.out.print("Enter the interest rate > ");
		interestRate = keyboard.nextDouble();
		keyboard.nextLine();
		
		// Calculate each year's future value
		year5 = investAmount * Math.pow(1 + interestRate, 5);
		year10 = investAmount * Math.pow(1 + interestRate, 10);
		year20 = investAmount * Math.pow(1 + interestRate, 20);
		
		// Display results
		System.out.println("With an investment of " + currency.format(investAmount) + " at an interest of " + decimal.format(interestRate) + " compounded annually:");
		System.out.println("\tthe future value in 5 years is " + currency.format(year5));
		System.out.println("\tthe future value in 10 years is " + currency.format(year10));
		System.out.println("\tthe future value in 25 years is " + currency.format(year20));
	}

}
