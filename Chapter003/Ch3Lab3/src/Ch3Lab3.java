/*
 * Program: Ch3Lab3.java
 * Programmer: Ray Osborne
 * Purpose: This program will have the user input meal costs, coupon discount and payment denomination. It will then calculate the amount of change
 */

import javax.swing.JOptionPane;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Ch3Lab3 {

	public static void main(String[] args) {
		// Declare variables used in Main method
		double meal, drink, dessert, coupon, total, denomination, centChange;
		int dollarChange;
		
		DecimalFormat decimal = new DecimalFormat(".00");
		NumberFormat currency = NumberFormat.getCurrencyInstance();
		DecimalFormat currency2 = new DecimalFormat("$##,##0");
		
		// Capture user input for this program
		meal = Double.parseDouble(JOptionPane.showInputDialog("What was the total cost of your meal?"));
		
		drink = Double.parseDouble(JOptionPane.showInputDialog("What was the price of your beverage?"));
		
		dessert = Double.parseDouble(JOptionPane.showInputDialog("What was the price of your dessert?"));
		
		coupon = Double.parseDouble(JOptionPane.showInputDialog("Do you have any coupons? Enter 0 for none or the amount of the coupon"));
		
		// Calculate the cost of the meal
		total = (meal + drink + dessert) - coupon;
		
		// Display the results
		JOptionPane.showMessageDialog(null, "Your meal was " + currency.format(meal) + "\n"
		+ "The cost of your drink was " + currency.format(drink) + "\n"
		+ "The cost of your dessert was " + currency.format(dessert) + "\n"
		+ "Coupon deduction is " + currency.format(coupon) + "\n"
		+ "The total cost of your meal is " + currency.format(total));
		
		// Get denomination customer is paying with
		denomination = Double.parseDouble(JOptionPane.showInputDialog("What is the denomination of the bill you will be paying with today?"));
		
		// Calculate customer's change
		dollarChange = (int)(denomination - total);
		centChange = (denomination - total) - dollarChange;
		
		// Get denomination customer is paying with
		JOptionPane.showMessageDialog(null, "Your change is " + currency2.format(dollarChange) + 
		" dollars and " + decimal.format(centChange) + " cents\n");
	}

}
