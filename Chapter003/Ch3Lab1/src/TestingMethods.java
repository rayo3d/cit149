/*
 * Program: TestingMethods.java
 * Programmer: Ray Osborne
 * Purpose: To test the charAt() method, substring(), trim(), indexOf(), toUpperCase(), toLowerCase(), length(), equalsIgnoreCase(), lastIndexOf()
 */

public class TestingMethods {

	public static void main(String[] args) {
		// Variables used in Main method
		String message, message2, message3;
		
		// Declare initial value to message
		message = "A long time ago, in a galaxy far, far, away";
		
		// Testing the length() method
		System.out.println(message.length());
		
		// Testing the equalsIgnoreCase()
		System.out.println(message.equalsIgnoreCase("Along time ago"));
		
		// Testing the toLowerCase() method
		System.out.println(message.toLowerCase());
		
		// Testing the toUpperCase() method
		System.out.println(message.toUpperCase());
		
		// Declare initial value to message2
		message2 = "      A long time ago, in a galaxy far, far, away        ";
		System.out.println(message2.trim());
		
		// Testing the charAt() method
		System.out.println(message2.charAt(10));
		System.out.println(message2.charAt(20));
		
		// Testing the substring() method
		System.out.println(message2.substring(10));
		System.out.println(message2.substring(25));
		
		// Testing the substring(Start, End) method
		System.out.println(message2.substring(10,25));
		System.out.println(message2.substring(5,30));
		
		// Testing the indexOf() method
		System.out.println(message2.indexOf("far"));
		System.out.println(message2.indexOf("Ray"));
		
		// Testing the indexOf(A_String, Star) method
		System.out.println(message2.indexOf("galaxy",10));
		System.out.println(message2.indexOf("Ray",2));
		
		// Testing the lastIndexOf() method
		System.out.println(message2.lastIndexOf("far"));
		System.out.println(message2.lastIndexOf("Ray"));
		
		// Declare initial value to message3
		message3 = message;
		
		// Testing the compareTo() method
		System.out.println(message3.compareTo("far"));
		
		// Compare different variables
		System.out.println(message3.compareTo(message2));
		System.out.println(message3.compareTo(message));
		
		// Create new variable for compareTo() testing
		String entry = "adventure";
		// Testing final variable
		System.out.println(entry.compareTo("zoo"));
	}

}
