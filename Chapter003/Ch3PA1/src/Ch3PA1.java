/*
 * Program: Ch3PA1.java
 * Programmer: Ray Osborne
 * Purpose: This program will randomly pick two numbers between 0-100 then print the smaller of the two numbers.
 */

import java.util.Random;

public class Ch3PA1 {

	public static void main(String[] args) {
		// Variables used in Main method
		Random number = new Random();
		int num1 = number.nextInt(100);
		int num2= number.nextInt(100);
		
		// Display numbers
		System.out.println("The numbers are " + num1 + " and " + num2 + ".");
		
		// Find smallest of the two then display
		if (num1 < num2) {
			System.out.println("The smaller number is " + num1 + ".");
		} else if (num1 > num2) {
			System.out.println("The smaller number is " + num2 + ".");
		} else {
			System.out.println("The numbers are equal.");
		}
	}

}
