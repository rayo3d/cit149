/**
 * 
 */

/**
 * @author Ray
 * @date 11-30-2015
 * @purpose Testing of Powerball Class for getting Powerball numbers
 */
public class PowerballTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed for Main method
		Powerball powerball = new Powerball();
		
		// Call method to capture data in Powerball class
		powerball.readInput();
		
		// Call method to display data in Powerball class
		powerball.writeOutput();
	}

}
