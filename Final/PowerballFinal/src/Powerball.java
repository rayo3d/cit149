/**
 * 
 */

/**
 * @author Ray
 * @date 11-30-2015
 * @purpose Class for Powerball number capture and verification of unique numbers
 */

import java.util.Arrays;
import java.util.Scanner;

public class Powerball {
	private Integer[] powerBallNumbers = new Integer[6];
	private int checkInputNum = 0;
	
	// Mutator method to change variable value
	public void setPowerballNumbers(Integer[] newPowerballNumbers) {
		powerBallNumbers = newPowerballNumbers;
	}
	
	// Accessor method to access variable value
	public Integer[] getPowerballNumbers() {
		return powerBallNumbers;
	}
	
	// Capture Powerball numbers from user
	public void readInput(){
		// Declare variables needed for readInput method
		Scanner keyboard = new Scanner(System.in);
		
		// Loop to populate Powerball array
		for (int pb = 0; pb < powerBallNumbers.length; pb++) {
			if (pb < (powerBallNumbers.length - 1)) {
				System.out.println("Please enter number " + (pb + 1) + " (number should be between 1 and 69): ");
				checkInputNum = keyboard.nextInt();
				
				// Check if number is outside Powerball ranges
				while (checkInputNum < 1 || checkInputNum > 69 || Arrays.asList(powerBallNumbers).indexOf(checkInputNum) > -1) {
					// Display appropriate error message
					if (checkInputNum < 1)
						System.out.println("\nNumber " + (pb + 1) + " must be greater than 0.");
					else if (checkInputNum > 69)
						System.out.println("\nNumber " + (pb + 1) + " must be less than than 70.");
					else if (Arrays.asList(powerBallNumbers).indexOf(checkInputNum) > -1) {
						System.out.println("\nNumber " + (pb + 1) + " must be unique and not previously entered.");
					}
					
					System.out.println("Please re-enter number " + (pb + 1) + " (number should be between 1 and 69): ");
					checkInputNum = keyboard.nextInt();
				}
				
				// Assign final input number to Powerball array and reset checkInputNum
				powerBallNumbers[pb] = checkInputNum;
				checkInputNum = 0;
				System.out.println();
			} else {
				System.out.println("Please enter number " + (pb + 1) + " for the lucky powerball number (number should be between 1 and 26): ");
				checkInputNum = keyboard.nextInt();
				
				// Check if number is outside lucky Powerball ranges
				while (checkInputNum < 1 || checkInputNum > 26) {
					// Display appropriate error message
					if (checkInputNum < 1)
						System.out.println("\nThe Powerball number must be greater than 0.");
					else if (checkInputNum > 26)
						System.out.println("\nThe Powerball number must be less than than 27.");
					
					System.out.println("Please re-enter number " + (pb + 1) + " for the lucky powerball number (number should be between 1 and 26: ");
					checkInputNum = keyboard.nextInt();
				}
				
				// Assign final input number to Powerball array and reset checkInputNum
				powerBallNumbers[pb] = checkInputNum;
				checkInputNum = 0;
				System.out.println();
			}
		}
	}
	
	// Display results
	public void writeOutput() {
		System.out.print("Your powerball numbers are ");
		for (int out = 0; out < powerBallNumbers.length; out++) {
			if (out < (powerBallNumbers.length - 1))
				System.out.print(powerBallNumbers[out] + ", ");
			else
				System.out.print("and the Powerball number is " + powerBallNumbers[out]);
		}
	}
}
