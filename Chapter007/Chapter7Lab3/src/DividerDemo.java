/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-5-2015
 * @purpose This program will call class to test division table
 */
public class DividerDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed for Main method
		int divider;
		double correct;
		
		Divider test = new Divider();
		
		// Call methods from Divider class
		test.welcome();
		divider = test.getDivider();
		correct = test.getCorrect();
		test.display();
	}

}
