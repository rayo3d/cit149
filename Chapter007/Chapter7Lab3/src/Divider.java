/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-5-2015
 * @purpose This class will setup testing on division tables
 */

import java.util.Scanner;

public class Divider {
	// Declare variables needed for Divider class
	private int divider;
	private double correct;
	
	// Display welcome message
	public void welcome() {
		System.out.println("\nTest your skills at Division without using a calculator.");
	}
	
	// Display request for user division table choice
	public int getDivider() {
		// Declare variable needed for getDivider method
		Scanner keyboard = new Scanner(System.in);
		
		// Ask user for division table to test
		System.out.println("\nEnter the division table you want to test yourself on:\n");
		divider = keyboard.nextInt();
		
		return divider;
	}
	
	// Track user correct answer
	public double getCorrect() {
		// Declare variables needed for getCorrect method
		double answer;
		double count = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Repeat question until user gets 13 correct
		while (count <= 12) {
			// Get user answer to division question
			System.out.println("\nWhat is " + count + " divided by " + divider + " (you must answer using 18 decimal places if the answer is not exact)?\n");
			answer = keyboard.nextDouble();
			
			// Check user answer
			if (answer == count/divider) {
				// Display correct message
				System.out.println("\nCorrect!");
				
				// Increase counters for correct answer
				count++;
				correct++;
			} else {
				// Display incorrect message
				System.out.println("\nIncorrect!");
			}
		}
		
		return correct;
	}
	
	// Display how many user got correct
	public void display() {
		System.out.println("\nYou got " + correct + " correct.\n");
	}
}
