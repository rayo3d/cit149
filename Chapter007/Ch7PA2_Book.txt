Write a class encapsulating the concept of a rational number, assum-
ing a rational number has the following attributes: an integer repre-
senting the numerator of the rational number, and another integer
representing the denominator of the rational number. Include a con-
structor, the accessors and mutators, and methods toString and equals.
You should not allow the denominator to be equal to 0; you should
give it the default value 1 in case the corresponding argument of the
constructor or a method is 0.Also include methods performing mul-
tiplication of a rational number by another and addition of a rational
number to another,returning the resulting rational number in both
cases.Write a client class to test all the methods in your class.