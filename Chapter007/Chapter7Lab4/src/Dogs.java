/**
 * 
 */

/**
* @author Ray Osborne
 * @date 11-5-2015
 * @purpose This class will assign user data for dog breeds
 */

import java.util.Scanner;

public class Dogs {
	// Declare variables needed for Dogs class
	private String dogName, dogClass, dogBreed;
	private int numberOfRibbons, numberOfShows;
	
	// Mutator methods to change variable values
	public void setName(String newName) {
		dogName = newName;
	}
	public void setClass(String newClass) {
		dogClass = newClass;
	}
	public void setBreed(String newBreed) {
		dogBreed = newBreed;
	}
	public void setNumberRibbons(int newNumberOfRibbons) {
		numberOfRibbons = newNumberOfRibbons;
	}
	public void setNumberShows(int newNumberOfShows) {
		if (newNumberOfShows <= 0)
			System.out.println("Please enter a number greater than zero. If this is the first show the dog has participated in enter 1.");
		else
			numberOfShows = newNumberOfShows;
	}
	
	// Request information from user
	public void readInput(){
		// Declare variables needed for readInput method
		Scanner keyboard = new Scanner(System.in);
		
		// Capture dog information
		System.out.println("Enter the name of the dog:");
		dogName = keyboard.nextLine();
		
		System.out.println("Enter class the dog participates in (sporting, toy, etc.):");
		dogClass = keyboard.nextLine();
		
		System.out.println("Enter the breed of the dog:");
		dogBreed = keyboard.nextLine();
		
		System.out.println("Enter the number of ribbons the dog has earned:");
		numberOfRibbons = keyboard.nextInt();
		
		System.out.println("Enter the number of show the dog has been in, including this one:");
		numberOfShows = keyboard.nextInt();
		
		// Check for valid number of shows input
		while (numberOfShows <= 0) {
			System.out.println("Number must be greater than zero.");
			System.out.println("Enter the number of show the dog has been in, including this one:");
			numberOfShows = keyboard.nextInt();
		}
	}
	
	// Display results
	public void writeOutput() {
		System.out.println("\n\nThe dog's name is " + dogName);
		System.out.println("The dog's class is " + dogClass);
		System.out.println("The dog's breed is " + dogBreed);
		System.out.println(dogName + " has won " + numberOfRibbons + " ribbons.");
		if (numberOfShows == 1)
			System.out.println("This is " + dogName + "'s first show.");
		else
			System.out.println(dogName + " has participated in " + numberOfShows + " shows.");
	}
	
	// Accessor methods to access variable values
	public String getName() {
		return dogName;
	}
	public String getClassOfDog() {
		return dogClass;
	}
	public String getBreed() {
		return dogBreed;
	}
	public int getNumberOfRibbons() {
		return numberOfRibbons;
	}
	public int getNumberOfShows() {
		return numberOfShows;
	}
}
