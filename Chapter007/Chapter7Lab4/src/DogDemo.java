/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-6-2015
 * @purpose This program will call class to capture dog information
 */
public class DogDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed for Main method
		String dogName, dogBreed, dogClass;
		int numberOfRibbons, numberOfShows;
		Dogs dogsInfo = new Dogs();
		
		// Call method to capture data in Dogs class
		dogsInfo.readInput();
		
		// Call method to display data in Dogs class
		dogsInfo.writeOutput();
		
		// Send data group 1 to Dogs class
		dogsInfo.setName("CD");
		dogsInfo.setClass("Working");
		dogsInfo.setBreed("Cairn Terrier");
		dogsInfo.setNumberRibbons(5);
		dogsInfo.setNumberShows(8);
		dogsInfo.writeOutput();
		
		// Send data group 2 to Dogs class
		dogsInfo.setName("Lassie");
		dogsInfo.setClass("Working");
		dogsInfo.setBreed("Collie");
		dogsInfo.setNumberRibbons(10);
		dogsInfo.setNumberShows(12);
		dogsInfo.writeOutput();
		
		// Send data group 3 to Dogs class
		dogsInfo.setName("Benji");
		dogsInfo.setClass("Mutt");
		dogsInfo.setBreed("Mutt");
		dogsInfo.setNumberRibbons(0);
		dogsInfo.setNumberShows(1);
		dogsInfo.writeOutput();
	}

}
