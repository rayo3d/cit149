/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-8-2015
 * @purpose This program will set and compare team names stored in a class
 */
public class TeamsDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables used in Main method
		Teams team1 = new Teams();
		Teams team2 = new Teams();
		
		// Set original team names
		team1.setTeamName("Orioles");
		team2.setTeamName("Yankees");
		
		// Call method to display team names
		team1.writeTeamName(Integer.toString(1));
		team2.writeTeamName(Integer.toString(2));
		
		// Compare Team Names
		while (!(team1.getTeamName().equals(team2.getTeamName()))) {
			System.out.println("Names of team #1 and #2 are different.");
			
			if (!(team1.getTeamName().equals(team2.getTeamName()))) {
				System.out.println("\nSetting team #2 to name " + team1.getTeamName());
				
				team2.setTeamName(team1.getTeamName());
			}
		}
		
		// Final output when team names are identical
		System.out.println("Names of team #1 and #2 are identical.");
	}

}
