/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-8-2015
 * @purpose This class will assign user data for teams
 */
public class Teams {
	// Declare variables needed for Teams class
	private String teamName;
	
	// Constructor for Teams class
	public Teams(String newTeamName) {
		teamName = newTeamName;
	}
	
	// Create empty constructor to allow access to Teams class
	public Teams() {
		
	}
	
	// Accessor method to access variable values
	public String getTeamName() {
		return teamName;
	}
	
	// Mutator method to change variable values
	public void setTeamName(String newTeamName) {
		teamName = newTeamName;
	}
	
	// Display Team names
	public void writeTeamName(String teamNameNum) {
		System.out.println("The name of team #" + teamNameNum + " is " + teamName + ".");
	}
}
