/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-5-2015
 * @purpose This program will capture and display user input information regarding checking and stock accounts
 */
public class BankDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// First Bank object to capture checking data from user
		Bank bank1 = new Bank();
		bank1.readCheckingInfo();
		
		// Second Bank object to pass in checking information directly
		Bank bank2 = new Bank("AB123", "Ray Osborne", 1000.00, 500.00);
		bank2.writeCheckingInfo();
		
		// Third Bank object to pass stock information directly
		Bank bank3 = new Bank("AB123", "Ray Osborne", "Microsoft", 5, 25.00);
		bank3.writeStockInfo();
		
		// Fourth Bank object to capture stock data from user
		Bank bank4 = new Bank();
		bank4.readStockInfo();
	}

}
