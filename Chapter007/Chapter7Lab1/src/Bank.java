/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-5-2015
 */

import java.text.NumberFormat;
import java.util.Scanner;

public class Bank {
	// Declare variables needed for Bank class
	private String accountNumber, accountName, stockName;	
	private double checkingDeposit, checkingWithdrawal, totalChecking, stockValue, totalStockValue, totalAccountValue;
	private int stockShares;
	
	private NumberFormat currency = NumberFormat.getCurrencyInstance();
	
	// Create Checking/Withdrawal constructor
	public Bank(String newAccountNumber, String newAccountName, double newCheckingDeposit, double newCheckingWithdrawal) {
		accountNumber = newAccountNumber;
		accountName = newAccountName;
		checkingDeposit = newCheckingDeposit;
		checkingWithdrawal = newCheckingWithdrawal;
	}
	
	// Create Stock constructor
	public Bank(String newAccountNumber, String newAccountName, String newStockName, int newStockShares, double newStockValue) {
		accountNumber = newAccountNumber;
		accountName = newAccountName;
		stockName = newStockName;
		stockShares = newStockShares;
		stockValue = newStockValue;
	}
	
	// Create empty constructor to allow access to Bank class
	public Bank() {
		
	}
	
	// Accessor methods to access variable values
	public String getAcountNumber() {
		return accountNumber;
	}
	public String getAccountName() {
		return accountName;
	}
	public String getStockName() {
		return stockName;
	}
	public double getCheckingDeposit() {
		return checkingDeposit;
	}
	public double getCheckingWithdrawal() {
		return checkingWithdrawal;
	}
	public double getStockValue() {
		return stockValue;
	}
	public int getStockShares() {
		return stockShares;
	}
	
	// Mutator methods to change variable values
	public void setAccountNumber(String newAccountNumber) {
		accountNumber = newAccountNumber;
	}
	public void setAccountName(String newAccountName) {
		accountName = newAccountName;
	}
	public void setStockName(String newStockName) {
		stockName = newStockName;
	}
	public void setCheckingDeposit(double newCheckingDeposit) {
		checkingDeposit = newCheckingDeposit;
	}
	public void setCheckingWithdrawal(double newCheckingWithdrawal) {
		checkingWithdrawal = newCheckingWithdrawal;
	}
	public void setStockValue(double newStockValue) {
		stockValue = newStockValue;
	}
	public void setStockShares(int newStockShares) {
		stockShares = newStockShares;
	}
	
	// Display Bank account information
	public void writeCheckingInfo() {
		// Update total checking amount
		totalChecking = checkingDeposit - checkingWithdrawal;
		
		// Display appropriate results
		if (totalChecking < 0)
			System.out.println("\nYou have overdrawn your checking account. A fee will be issued.");
		else
			System.out.println("There is a balance of " + currency.format(totalChecking) + " in account number " + accountNumber + " under the name of " + accountName + ".\n");
	}
	
	// Display Bank stock information
	public void writeStockInfo() {
		// Update total stock value amount
		totalStockValue = stockShares * stockValue;
		
		// Display results
		System.out.println("\nAccount Number " + accountNumber + " under the name of " + accountName + " has purchase " + stockShares + " shares of " + stockName + " stock for a total value of " + currency.format(totalStockValue) + ".\n");
	}
	
	// Request user checking information
	public void readCheckingInfo() {
		// Declare variables needed for readCheckingInfo method
		char answer;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Capture user data
		System.out.println("Please enter the account number: ");
		accountNumber = keyboard.nextLine();
		
		System.out.println("Please enter the account name (first name/last name): ");
		accountName = keyboard.nextLine();
		
		System.out.println("What are you depositing into your checking account today? ");
		checkingDeposit = keyboard.nextDouble();
		
		System.out.println("Are you making any withdrawals? ");
		answer = keyboard.next().charAt(0);
		
		// Check if user is making withdrawal
		if ((answer == 'Y') || (answer == 'y')) {
			System.out.println("Please enter the amount of your withdrawal: ");
			checkingWithdrawal = keyboard.nextDouble();
		} else
			checkingWithdrawal = 0;
		
		// Call method to display checking information
		writeCheckingInfo();
	}
	
	// Request user stock information
		public void readStockInfo() {
			// Declare variables needed for readCheckingInfo method
			Scanner keyboard = new Scanner(System.in);
			
			// Capture user data
			System.out.println("Please enter the account number: ");
			accountNumber = keyboard.nextLine();
			
			System.out.println("Please enter the account name (first name/last name): ");
			accountName = keyboard.nextLine();
			
			System.out.println("What is the stock you are purchasing shares in? ");
			stockName = keyboard.nextLine();
			
			System.out.println("How many shares of " + stockName + " have you purchased? ");
			stockShares = keyboard.nextInt();
			
			System.out.println("What was the current per share cost of " + stockName + "? ");
			stockValue = keyboard.nextDouble();
			
			// Call method to display stock information
			writeStockInfo();
		}
}
