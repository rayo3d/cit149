/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-5-2015
 * @purpose This class will test different methods of the Math class
 */

import java.util.Scanner;
import javax.swing.JOptionPane;

public class MathTest {
	// Declare variables needed for MathTest class
	private static int x = 1;
	
	private static Scanner keyboard = new Scanner(System.in);
	
	// Multiply a number by pi and check to if user got answer correct
	public static void getPi() {
		// Declare variables needed for getPi method
		double answer, number = 10;
		
		// Capture user answer to multiplication question
		answer = Double.parseDouble(JOptionPane.showInputDialog(null, "What is " + number + " * " + Math.PI + "?"));
		
		// Check user answer
		if (answer == Math.PI * number) 
			JOptionPane.showMessageDialog(null, "You are correct!");
		else
			JOptionPane.showMessageDialog(null, "Sorry but " + (Math.PI * number) + " is the correct answer.");
	}
	
	// Ask user to pick the smallest between two numbers
	public static void getMinimum() {
		// Declare variables needed for getMinimum method
		int answer, number1 = -10, number2 = -2;
		
		// Capture user answer to minimum number question
		answer = Integer.parseInt(JOptionPane.showInputDialog(null, "What number is smaller? " + number1 + " or " + number2 + "\nType in your choice: "));
		
		// Check user answer
		if (answer == Math.min(number1, number2)) 
			JOptionPane.showMessageDialog(null, "You are correct!");
		else
			JOptionPane.showMessageDialog(null, "Sorry but " + Math.min(number1, number2) + " is the correct answer.");
	}
	
	// Ask user to pick the largest between two numbers
	public static void getMaximum() {
		// Declare variables needed for getMinimum method
		int answer, number1 = -4, number2 = -15;
		
		// Capture user answer to minimum number question
		answer = Integer.parseInt(JOptionPane.showInputDialog(null, "What number is greater? " + number1 + " or " + number2 + "\nType in your choice: "));
		
		// Check user answer
		if (answer == Math.max(number1, number2)) 
			JOptionPane.showMessageDialog(null, "You are correct!");
		else
			JOptionPane.showMessageDialog(null, "Sorry but " + Math.max(number1, number2) + " is larger than " + number2 + ".");
	}
	
	// Ask user for the smallest integer larger than the given number
	public static void getCeil() {
		// Declare variables needed for getCeil method
		double number = 2.62;
		int answer;
		
		// Capture user answer to smallest integer
		answer = Integer.parseInt(JOptionPane.showInputDialog(null, "What is the smallest integer greater than " + number + "?"));
		
		// Check user answer
		if (answer == Math.ceil(number)) 
			JOptionPane.showMessageDialog(null, "You are correct!");
		else
			JOptionPane.showMessageDialog(null, "Sorry but " + Math.ceil(number) + " is the correct answer.");
	}
	
	// Ask user for the largest integer less than the given number
	public static void getFloor() {
		// Declare variables needed for getFloor method
		double number = 2.62;
		int answer;
		
		// Capture user answer to largest integer
		answer = Integer.parseInt(JOptionPane.showInputDialog(null, "What is the largest integer less than " + number + "?"));
		
		// Check user answer
		if (answer == Math.floor(number)) 
			JOptionPane.showMessageDialog(null, "You are correct!");
		else
			JOptionPane.showMessageDialog(null, "Sorry but " + Math.floor(number) + " is the correct answer.");
	}
	
	// Test user on power of a number using a loop
	public static void getPowerValue() {
		// Declare variables needed for getPowerValue
		int number1 = 0, number2 = 1;
		double answer;
		
		// Loop to test user on power of numbers
		for (int i = number1; i <= 5; i++) {
			// Capture user answer to power of numbers question
			answer = Double.parseDouble(JOptionPane.showInputDialog(null, "What is the value of n to the " + number1 + " to the " + number2 + " power?"));
			
			// Check user answer
			if (answer == Math.pow(number1, number2)) 
				JOptionPane.showMessageDialog(null, "You are correct!");
			else
				JOptionPane.showMessageDialog(null, "Sorry but " + Math.pow(number1, number2) + " is the correct answer.");
			
			// Increase values for next question
			number1++;
			number2++;
		}
	}
	
	// Ask what absolute value of a number
	public static void getAbsoluteValue() {
		// Declare variables needed for getAbsoluteValue method
		double absoluteValue, answer;
		
		// Loop to test user on absolute value
		for (int i = 0; i <= 10; i++) {
			// Update answer to test against
			answer = (2 * (2 * x - 2) - 3);
			
			// Capture user answer to absolute value question
			absoluteValue = Double.parseDouble(JOptionPane.showInputDialog(null, "What is the absolute value of (2(2x - 2) - 3) if given x = " + x + "?"));
			
			// Check user answer
			if (absoluteValue == Math.abs(answer)) 
				JOptionPane.showMessageDialog(null, "You are correct!");
			else
				JOptionPane.showMessageDialog(null, "Sorry! The absolute value of " + answer + " is " + Math.abs(answer) + ".");
			
			// Increase value for next question
			x++;
		}
	}
	
	// Ask user to round the value of a number divided by another number
	public static void getRoundValue() {
		// Declare variables needed for getRoundValue method
		double numerator = 4, denominator = 2, answer;
		
		// Loop to test user on rounded value
		for (int i = 0; i <= 12; i++) {
			// Capture user answer to rounding question
			answer = Double.parseDouble(JOptionPane.showInputDialog(null, "Round off " + numerator + "/" + denominator + "."));
						
			// Check user answer
			if (answer == Math.round(numerator / denominator)) 
				JOptionPane.showMessageDialog(null, "You are correct!");
			else
				JOptionPane.showMessageDialog(null, "Sorry but " + numerator + "/" + denominator + " rounds off to " + Math.round(numerator / denominator) + ".");
			
			// Increase value for next question
			numerator++;
			denominator++;
		}
	}
	
	// Ask user to square root several numbers
	public static void getSquareRoot() {
		// Declare variables needed for getSquareRoot method
		double number = 0.235, answer;
		
		// Loop to test user on square roots
		for (int i = 0; i <= 20; i++) {
			// Capture user answer to square root question
			answer = Double.parseDouble(JOptionPane.showInputDialog(null, "What is the square root of " + number + " rounded to the nearest whole number?"));
						
			// Check user answer
			if (answer == Math.round(Math.sqrt(number))) 
				JOptionPane.showMessageDialog(null, "You are correct!");
			else
				JOptionPane.showMessageDialog(null, "Sorry but the square root of " + number + " is " + (Math.round(Math.sqrt(number))) + ".");
			
			// Increase value for next question
			number = number + 3.6325;
		}
	}
}
