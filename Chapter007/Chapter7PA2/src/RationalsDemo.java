/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-8-2015
 * @purpose This program will test concepts of rational numbers
 */
public class RationalsDemo {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Declare variables and objects needed in Main method
		Rationals rational1, rational2, result;
		
		// Set values for each object
		rational1 = new Rationals(2, 3);
		rational2 = new Rationals(5, 7);
		
		// Display rational numerators and denominators
		System.out.println("The numberator for rational #1 is " + rational1.getNumerator());
		System.out.println("The numberator for rational #2 is " + rational1.getDenominator());
		System.out.println("Rational #1 is " + rational2.toString());
		
		// Multiply and add rational numbers
		System.out.println("\n" + rational1.toString() + " * " + rational2.toString() + " = " + rational1.multiply(rational2));
		System.out.println(rational1.toString() + " + " + rational2.toString() + " = " + rational1.add(rational2));
	}

}
