/**
 * 
 */

/**
 * @author Ray Osborne
 * @date 11-8-2015
 * @purpose This class will demonstrate the concept of rational numbers
 */
public class Rationals {
	// Declare variables needed for Rationals class
	private int numerator, denominator;
	
	// Create empty constructor to allow access to Rationals class
	public Rationals(int newNumerator, int newDenominator) {
		// Check if newDenominator equals zero
		if (newDenominator != 0)
			denominator = newDenominator;
		else {
			System.out.println("\n\nYou cannot divide by zero. Resetting denominator to 1.");
			denominator = 1;
		}
		
		numerator = newNumerator;
	}
	
	// Accessor methods to access variable values
	public int getNumerator() {
		return numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	
	// Mutator methods to change variable values
	public void setNumerator(int newNumerator) {
		numerator = newNumerator;
	}
	public void setDenominator(int newDenominator) {
		// Check if newDenominator equals zero
		if (newDenominator != 0)
			denominator = newDenominator;
		else {
			System.out.println("\n\nYou cannot divide by zero. Resetting denominator to 1.");
			denominator = 1;
		}
	}
	
	// Return formated numerator and denominator
	public String toString() {
	   return numerator + "/" + denominator;
	}
	
	// Check if object is an instance of class Rationals
	public boolean equals(Object checkObj) {
		if (!(checkObj instanceof Rationals))
			return false;
		else {
			Rationals passObj = (Rationals) checkObj;
			
			return true;
		}
	}
	
	// Multiply two rationals and return results
	public Rationals multiply(Rationals r2) {
		return new Rationals(numerator * r2.numerator, denominator * r2.denominator);
	}
	
	// Add two rationals and return results
	public Rationals add(Rationals r2) {
		int num   = (numerator * r2.denominator) + (denominator * r2.numerator);
		int denom = denominator * r2.denominator;
		return new Rationals(num, denom);
	}
}
