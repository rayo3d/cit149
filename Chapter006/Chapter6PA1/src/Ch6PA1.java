/*
 * Date: October-22-2015
 * Programmer: Ray Osborne
 * Purpose: Ask user for a number greater than 10 then count number of times required to square root number to less than 1.01
*/

import java.util.Scanner;

public class Ch6PA1 {

	public static void main(String[] args) {
		// Initialize variables needed for Main method
		double userNum = 0;
		int sqrCounter = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Ask for user input and repeat until user enters a value greater than 10
		while (userNum <= 10) {
			// Capture user number
			System.out.println("Enter a number greater than 10: ");
			userNum = keyboard.nextDouble();
			
			// Check if number is greater than 10 and display message if not
			if (userNum <= 10)
				System.out.println("\nPlease enter another number that is greater than 10.\n");
		}
		
		// Repeat finding square root of user number and it's results until value is less than 1.01 and count 
		do {
			userNum = Math.sqrt(userNum);
			
			sqrCounter++;
		} while (userNum > 1.01);
		
		// Display final count of square roots
		System.out.println("The number of square roots is " + sqrCounter);
	}

}
