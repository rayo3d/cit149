/*
 * Date: October-22-2015
 * Programmer: Ray Osborne
 * Purpose: Loop to ask for grades then display all grades entered and the average
*/

import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Ch6Lab3 {

	public static void main(String[] args) {
		// Initialize variables needed in Main method
		int numberOfGrades;
		double grade, totalGrades = 0, average;
		
		Scanner input = new Scanner(System.in);
		
		
		// Capture number of grades to be entered
		System.out.println("How many grades will you enter?");
		numberOfGrades = input.nextInt();
		
		// For loop to capture grades based on number to enter
		for (int i = 1; i <= numberOfGrades; i++) {
			// Capture grade for i
			System.out.println("\nEnter grade " + i + ": ");
			grade = input.nextDouble();
			
			// Add to totalGrades
			totalGrades = totalGrades + grade;
		}
		
		// Calculate average of grades
		average = totalGrades / numberOfGrades;
		
		// Display results
		System.out.println("\n\nThe total grades are " + totalGrades + " and the average grade is " + average);
	}

}
