/*
 * Date: October-22-2015
 * Programmer: Ray Osborne
 * Purpose: Ask user for a positive number and find factorial of that number
*/

import java.util.Scanner;

public class Ch6PA2 {

	public static void main(String[] args) {
		// Initialize variables needed for Main method
		int userNum = 0, factorialNum = 1;
		
		Scanner keyboard = new Scanner(System.in);
		
		// Ask for user input and repeat until user enters a positive
		while (userNum < 1) {
			// Capture user number
			System.out.println("Enter a positive integer: ");
			userNum = keyboard.nextInt();
			
			// Check if number is greater than 10 and display message if not
			if (userNum < 1)
				System.out.println("\nPlease enter a positive number.\n");
		}
		
		// Loop to find factorial value of user input
		for (int cnt = 1; cnt <= userNum; cnt++) {
			factorialNum  = factorialNum * cnt;
		}
		
		// Display final factorial
		System.out.println("The factorial of " + userNum + " is " + factorialNum);
	}

}
