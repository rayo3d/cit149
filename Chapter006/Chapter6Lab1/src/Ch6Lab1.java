/*
 * Date: October-21-2015
 * Programmer: Ray Osborne
 * Purpose: Test while loop by giving user choice to exit program
*/

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Ch6Lab1 {

	public static void main(String[] args) {
		// Declare variables used in Main method
		double salary;
		int answer;
		boolean quitProgram = false;
		
		Scanner keyboard = new Scanner(System.in);
		
		// While loop to check for quit parameter
		while (quitProgram == false) {
			// Ask for user salary
			System.out.println();
			System.out.println("Enter your current monthly salary: ");
			salary = keyboard.nextDouble();
			
			// Display appropriate message based on salary
			if (salary >= 4700)
				JOptionPane.showMessageDialog(null, "A salary of $" + salary + " isn't chicken feed");
			else if (salary >= 2100.00 && salary < 4700)
				JOptionPane.showMessageDialog(null, "A salary of $" + salary + " isn't too bad. Could be better");
			else
				JOptionPane.showMessageDialog(null, "You need a raise!");
			
			// Check if user wants to exit program
			answer = JOptionPane.showConfirmDialog(null, "End program?", "Request to End Program", JOptionPane.YES_NO_OPTION);
			
			if (answer == JOptionPane.YES_OPTION) {
				quitProgram = true;
			}
		}
	}

}
