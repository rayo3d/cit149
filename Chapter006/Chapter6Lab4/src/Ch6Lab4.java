/*
 * Date: October-22-2015
 * Programmer: Ray Osborne
 * Purpose: Draw several hollow circles
*/

import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Color;

public class Ch6Lab4 extends JFrame {

	public static void main(String[] args) {
		// Create class instance, set frame size, title and visiblity
		Ch6Lab4 frame = new Ch6Lab4();
		frame.setSize(500, 500);
		frame.setVisible(true);
		frame.setTitle("Spiraling circles");
	}

	public void paint(Graphics g) {
		// Set initial values for Paint method
		int x = 300, y = 300;
		Circle c;
		
		Color toggleColor = Color.BLACK;
		
		// For loop to create multiple circles
		for (int diameter = 300; diameter >= 10; diameter -= 10) {
			// Draw circle
			c = new Circle(x - diameter / 2, y - diameter / 2, diameter, toggleColor);
			c.draw(g);
			
			// Adjustments for next circle
			x = x - 20;
			y = y - 20;
			diameter = diameter - 10;
			
			// Change circle color based on current color
			if (toggleColor.equals(Color.BLACK))
				toggleColor = Color.GREEN;
			else if (toggleColor.equals(Color.GREEN))
				toggleColor = Color.BLUE;
			else if (toggleColor.equals(Color.BLUE))
				toggleColor = Color.RED;
			else
				toggleColor = Color.BLACK;
		}
	}
}
